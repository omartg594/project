const { request, response } = require('express');
const numbers = require('../constants/numbers')

const holaMundo = (req = request, res = response) =>{

    res.json({
        msg: 'Hola desde el servidor de Node con una funcion en controller',
        numero: numbers.FIVE,
    });
};

module.exports = {
    holaMundo
};