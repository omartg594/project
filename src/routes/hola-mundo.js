const { Router, request, response } = require('express');
const { holaMundo } = require('../controllers/hola-mundo');

const router = Router();

router.get('/hola-mundo', holaMundo);

module.exports = router;