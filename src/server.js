const express = require('express');

class Server {
    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.middlewares();
        this.routes();
    }

    middlewares(){
        this.app.use(express.json());
    }

    routes() {
        this.app.use('/api/saludos', require('../src/routes/hola-mundo'))
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log('Run server on port', this.port);
        });
    }
}

module.exports = Server;